<?php
function internet_reception_page_form($form, $form_state) {
  $form = array();
  $form['#prefix'] = '<div id="wizard-form-wrapper">';
  $form['#suffix'] = '</div>';
  $form['feedback_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Full name'),
    '#size' => 50,
    '#required' => TRUE,
);
  $form['feedback_age'] = array(
    '#type' => 'textfield',
    '#title' => t('Age'),
    '#size' => 3,
    '#required' => TRUE,
    '#element_validate' => array('element_validate_integer_positive'),
);
  $form['feedback_name_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Topic message'),
    '#size' => 50,
    '#required' => TRUE,
);
  $form['feedback_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#resizable' => FALSE,
    '#rows' => 5,
    '#required' => TRUE,
);
  $form['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Send'),
    '#ajax' => array(
      'callback' => 'internet_reception_form_callback',
      'wrapper' => 'wizard-form-wrapper',
      'effect' => 'fade',   
    )
  );
    return $form;
}
function internet_reception_form_callback($form, $form_state){
  return $form;
}
/**
 * Submit function for internet_reception_page_form.
 */
function internet_reception_page_form_submit($form, &$form_state) {
  $to = 'ermak321@gmail.com';
  $head = 'There is a new question.';
  $from = variable_get('system_mail', 'drupal@mail.ru');
  $my_module = 'internet_reception';
  $token = 'quest';
  $message = array (
    'id' => $my_module  . '_' . $token,
    'to' => 'ermak321@gmail.com',
    'subject' => 'New question!',
    'body' => '<p>My name is ' . $form_state['values']['feedback_name'] . ',</p>
               <p>Me ' . $form_state['values']['feedback_age'] . ' year,</p>
               <p>Have a question: ' . $form_state['values']['feedback_name_message'] . ', </p>
               <p>Message: ' . $form_state['values']['feedback_message'] . '.</p>',
    'headers' => array (
      'From' => $from,
      'Sender' => $from,
      'Return-Path' => $from,
      'Content-Type' => 'text/html; charsen=UTF-8; format=flowed; delsp=yes;',
    ),
  );
  $system = drupal_mail_system($my_module, $token);
  if ($system->mail($message)) {
  drupal_set_message(t("Your message send!"));
  }
}