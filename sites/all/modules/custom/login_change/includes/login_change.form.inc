<?php

/**
 * Callback function for login_change_form_alter.
 */

function login_change_form_alter_callback($form, &$form_state) {
  global $user;
    $user = user_load($form_state['uid']);
    $form_state['redirect'] = 'user/' . $user->uid;
    user_login_finalize($form_state);
    drupal_set_message('success');
  return $form['#prefix'].$form['#suffix'];
}